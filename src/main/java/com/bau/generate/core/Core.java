/**
 * Created By Fitriyadi
 * Date :23/05/2022
 * Time :11:59
 * Project Name : generatorbau
 * Email : fitriyadhie@gmail.com
 */

package com.bau.generate.core;

import com.bau.generate.util.MapBAU;
import com.bau.generate.util.MapFieldBAU;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.HashMap;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

public class Core {

    public static int findRow(Sheet sheet, String cellContent) {
        for (Row row : sheet) {
            for (Cell cell : row) {
                if (cell.getCellType() == STRING) {
                    if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
                        return row.getRowNum();
                    }
                }
                if (cell.getCellType() == NUMERIC) {
                    String matrixCode = String.valueOf(cell.getNumericCellValue());

                    if (matrixCode.substring(0,matrixCode.length()-2).equals(cellContent)) {
                        return row.getRowNum();
                    }
                }
            }
        }
        return 0;
    }

    public static int findRowCode(Sheet sheet, String cellContent) {
        for (Row row : sheet) {
            for (Cell cell : row) {
                if(cell.getColumnIndex()==3) {
//                    System.out.println("zxcz"+cell.getNumericCellValue());
                    if (cell.getCellType() == STRING) {
                        if (cell.getRichStringCellValue().getString().trim().toUpperCase().equals(cellContent.toUpperCase())) {
                            return row.getRowNum();
                        }
                    }
                    if (cell.getCellType() == NUMERIC) {
                        String matrixCode = String.valueOf(cell.getNumericCellValue());

                        if (matrixCode.substring(0, matrixCode.length() - 2).equals(cellContent)) {
                            return row.getRowNum();
                        }
                    }
                }
            }
        }
        return 0;
    }

    public static int findCell(Sheet sheet, String cellContent) {
        for (Row row : sheet) {
            for (Cell cell : row) {
                if (cell.getCellType() == STRING) {
                    if (cell.getRichStringCellValue().getString().trim().toUpperCase().equals(cellContent.toUpperCase())) {
                        return cell.getColumnIndex();
                    }
                }
                if (cell.getCellType() == NUMERIC) {
                    String matrixCode = String.valueOf(cell.getNumericCellValue());

                    if (matrixCode.substring(0,matrixCode.length()-2).equals(cellContent)) {
                        return cell.getColumnIndex();
                    }
                }
            }
        }
        return 0;
    }

    public static String getValue(String key){
        HashMap<String,String> mapCategory = MapBAU.categoryMap();
        for(HashMap.Entry<String, String> entry : mapCategory.entrySet()){

            if(entry.getKey().trim().toLowerCase().replaceAll("\\s", "").contains(key.toLowerCase().replaceAll("\\s", ""))){
                return entry.getValue().toString();
            }
        }
        return key;
    }

    public static String getFieldValue(String key){
        HashMap<String,String> mapCategory = MapFieldBAU.fieldMap();
        for(HashMap.Entry<String, String> entry : mapCategory.entrySet()){

            if(entry.getKey().trim().toLowerCase().replaceAll("\\s", "").contains(key.toLowerCase().replaceAll("\\s", ""))){
                return entry.getValue().toString();
            }
        }
        return key;
    }

    public static String getValueFromExcel(Cell cell){
        String code = "";
        switch (cell.getCellType()) {
            case STRING:
                code = Core.getValue(cell.getRichStringCellValue().getString());
                break;
            case NUMERIC:
                code = Core.getValue(String.valueOf(cell.getNumericCellValue()));
                break;
            case BOOLEAN:
                code = String.valueOf(cell.getBooleanCellValue());
            case FORMULA:
                code = "Formula";
                break;
            case BLANK:
                code="";
                break;
//                default: data.get(new Integer(i)).add(" ");
        }
        return code;
    }
}
