/**
 * Created By Fitriyadi
 * Date :23/05/2022
 * Time :11:59
 * Project Name : generatorbau
 * Email : fitriyadhie@gmail.com
 */

package com.bau.generate.matrix;

import com.bau.generate.core.Core;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class CreateSqlMatrixXLPostPaid {

    //https://drive.google.com/drive/folders/1hsyWJS3ztXvZFAy2OxyThV3kDZ3Pq7Jp hasil download
    private static String PATH_FILE = "C:\\1_Mine\\1Work\\BAU\\Doc\\template\\";
    private static String FILE_NAME = "Product Concept XL POSTPAID Matrix 02062022.xlsx";
    private static String MATRIX_CODE = "21";
    private static String PRODUCT_STREAM = "POSTPAID";
    private static String fieldName, view, mandatory, edit;

    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream(new File(PATH_FILE+FILE_NAME));
        Workbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);
        Row rowNum = sheet.getRow(Core.findRow(sheet,MATRIX_CODE));

        String subProduct="",reqType="",reqDetail="";
        //getValueFromExcel mapping at util.MapBAU
        for (Cell cell : rowNum) {
            if (cell.getColumnIndex() == 0) {//sub product
                subProduct = Core.getValueFromExcel(cell);
            } else if (cell.getColumnIndex() == 1) {//req type
                reqType = Core.getValueFromExcel(cell);
            } else if (cell.getColumnIndex() == 2) {//req detail
                reqDetail = Core.getValueFromExcel(cell);
            }
        }

        Sheet sheet2 = workbook.getSheetAt(1);
        int rowSize=6;
        Iterator<Row> row = sheet2.rowIterator();    //iterating over excel file

        int colCode = Core.findCell(sheet2,MATRIX_CODE);
        while (row.hasNext()) {
            Row rowNumSheet2 = row.next();

            if (rowNumSheet2.getRowNum()>=rowSize) {
                Iterator<Cell> cellIterator = rowNumSheet2.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell2 = cellIterator.next();
                    if (cell2.getColumnIndex() == 1) {//Field Name
                        fieldName = Core.getFieldValue(cell2.getRichStringCellValue().getString());
                    }else if (cell2.getColumnIndex() == colCode) {//View
                        view = cell2.getRichStringCellValue().getString();
                    }
                    else if (cell2.getColumnIndex() == colCode+1) {//Mandatory
                        mandatory = cell2.getRichStringCellValue().getString();
                    }
                    else if (cell2.getColumnIndex() == colCode+2) {//Edit
                        edit = cell2.getRichStringCellValue().getString();
                    }
                }
                if (!view.isEmpty()){
                    String sqlGen=String.format("SELECT public.insertfieldaccessmatrix" +
                            "(\'%s\',\'%s\',\'%s\',\'%s\',\'%s\'," +
                            "\'%s-%s-%s\',false, true, false);"
                            ,PRODUCT_STREAM,subProduct,reqType,reqDetail,fieldName,
                            view,mandatory,edit);

                    System.out.println(sqlGen);
                }

            }
        }


    }
//SELECT public.insertfieldaccessmatrix
// ('PREPAID','BANK', 'REVISE_PACKAGE','NOTIFICATION','MpeNo','Y-Y-Y', false, true, false);


}
