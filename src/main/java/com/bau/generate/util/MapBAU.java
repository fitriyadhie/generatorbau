/**
 * Created By Fitriyadi
 * Date :23/05/2022
 * Time :11:59
 * Project Name : generatorbau
 * Email : fitriyadhie@gmail.com
 */

package com.bau.generate.util;

import java.util.HashMap;

public class MapBAU {
    public static HashMap<String, String> categoryMap() {
        HashMap<String, String> map = new HashMap<String, String>();//Creating HashMap
        map.put("123-Sosy", "SOSY");
        map.put("Bank/Settlement", "BANK");
        map.put("Revise Package", "REVISE_PACKAGE");
        map.put("Revise", "REVISE");
        map.put("Notification", "NOTIFICATION");
        map.put("Notification-123", "NOTIFICATION-123");
        map.put("New SOC CD", "CREATE_NEW_SOCD");
        map.put("Create new packet with Existing SOC CD", "CREATE_EXIST_SOCD");
        map.put("Regular", "REGULAR");
        map.put("151.0", "151");
        map.put("Product Information-123", "PRODUCT_INFO-123");
        map.put("Roaming -123","ROAMING-123");
        map.put("Compliment", "COMPLIMENT");
        map.put("Booster", "BOOSTER");
        map.put("First Event", "FIRST_EVENT");
        map.put("Bundling", "BUNDLING");
        map.put("Benefit", "BENEFIT");


        //Axis
        map.put("123-Legacy", "LEGACY");
        map.put("Create From Scratch", "CREATE");
        map.put("SPJIT (mechanism masa tunggu 60hr)", "SPJIT");

        //map filename prodStream
        map.put("Product Concept XL PREPAID Matrix 20052021.xlsx", "PREPAID");
        map.put("Product Concept AXIS Matrix 20052021.xlsx", "AXIS");

        return map;
    }
}
