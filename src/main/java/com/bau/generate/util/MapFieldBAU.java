/**
 * Created By Fitriyadi
 * Date :23/05/2022
 * Time :11:59
 * Project Name : generatorbau
 * Email : fitriyadhie@gmail.com
 */

package com.bau.generate.util;

import java.util.HashMap;

public class MapFieldBAU {
    public static HashMap<String, String> fieldMap() {
        HashMap<String, String> map = new HashMap<String, String>();//Creating HashMap
        map.put("MPE No.", "MpeNo");
        map.put("Project Name", "ProjectName");
        map.put("Business Objective", "BussinessObjective");
        map.put("Program Description", "ProgramDescription");
        map.put("Timebox (Days)", "TimeBox");
        map.put("Number of Benefit", "NumberOfBenevit");
        map.put("Packet Name", "ProductName");
        map.put("Duplicated SOC CD", "AO_SOC_CD");
        map.put("New SOC CD", "NewSocCd");
        map.put("UPCC SOC CD", "UPCC_SOC_CD");
        map.put("OTC/REC", "SubscriptionType");
        map.put("CD_MAIN_ID_REG/RMD_MAIN_ID", "CD_MAIN_ID");
        map.put("Variant", "Variant");
        map.put("CD Main ID Rec", "CD_MAIN_ID_REC");
        map.put("Reference Service ID", "ServiceID");
        map.put("Waiting Period", "WaitingPeriod");
        map.put("Validity Reg (Days)", "EXPIRATION_DAYS_REG");
        map.put("Validity Rec (Days)", "EXPIRATION_DAYS_REC");
        map.put("Price Reg (Rupiah)", "REGISTRATION_FEE_REG");
        map.put("Price Rec (Rupiah)", "REGISTRATION_FEE_REC");
        map.put("Single Active Time", "SingleActiveTime");
        map.put("Name in Single DB", "NameInSingleDb");
        map.put("Original Price", "OriginalPrice");
        map.put("Adjust Balance Dompul", "AdjustBalanceDompul");
        map.put("UMB Name for URL", "UMBNameForUrl");
        map.put("Keyword Status", "SMSKeywordStatus");
        map.put("Keyword", "SMSKeyword");
        map.put("Create 2 platform", "Create2Platform");
        map.put("Recurring Migrate", "RecurringMigrate");
        map.put("Multipurchase Same Service_ID", "MultipurchaseSameServiceId");
        map.put("Multipurchase Same SOCCD", "MultipurchaseSameSOCCD");
        map.put("Unreg Packet", "UnregPacket");
        map.put("1st event packet ", "FirstEventPacket");
        map.put("Timeband", "Timeband");
        map.put("Level Up", "LevelUp");
        map.put("Product Code", "ProductCode");
        map.put("Convert Price to Quota", "ConvertPriceToQuota");
        map.put("Convert Quota to Package", "ConvertQuotaToPackage");

        //PostPaid
        map.put("MPE No.","MpeNo");
        map.put("Project Name","ProjectName");
        map.put("Business Objective","BussinessObjective");
        map.put("Program Description","ProgramDescription");
        map.put("Timebox (Days)","TimeBox");
        map.put("Number of Benefit","NumberOfBenevit");
        map.put("Item ID Name","ItemIdName");
        map.put("Packet Name","ProductName");
        map.put("Duplicated SOC CD","AO_SOC_CD");
        map.put("New SOC CD","NewSocCd");
        map.put("UPCC SOC CD","UPCC_SOC_CD");
        map.put("OTC/REC","SubscriptionType");
        map.put("CD MAIN ID REG/RMD MAIN ID","CD_MAIN_ID");
        map.put("Variant","Variant");
        map.put("CD Main ID Rec","CD_MAIN_ID_REC");
        map.put("Reference Service ID","ServiceID");
        map.put("Waiting Period x","WaitingPeriod");
        map.put("Validity Reg (Days)","EXPIRATION_DAYS_REG");
        map.put("Validity Rec (Days)","EXPIRATION_DAYS_REC");
        map.put("Price Reg (Rupiah)","REGISTRATION_FEE_REG");
        map.put("Price Rec (Rupiah)","REGISTRATION_FEE_REC");
        map.put("Include Tax","IncludeTax");
        map.put("Single Active Time","SingleActiveTime");
        map.put("Name in Single DB","NameInSingleDb");
        map.put("Original Price","OriginalPrice");
        map.put("Adjust Balance Dompul","AdjustBalanceDompul");
        map.put("UMB Name for URL","UMBNameForUrl");
        map.put("Keyword Status","SMSKeywordStatus");
        map.put("Keyword","SMSKeyword");
        map.put("Max Rec Count","MaxRecCount");
        map.put("Enable Flex","EnableFlex");
        map.put("Voucher Code","VoucherCode");
        map.put("Charge Code","ChargeCode");
        map.put("Recurring Migrate","RecurringMigrate");
        map.put("Multipurchase Same Service_ID","MultipurchaseSameServiceId");
        map.put("Multipurchase Same SOCCD","MultipurchaseSameSOCCD");
        map.put("Unreg Packet","UnregPacket");
        map.put("Timeband","Timeband");
        map.put("Max Purchase Per BC","MaxPurchaseBC");
        map.put("Send 2nd Notif","Send2ndNotif");
        map.put("Discount Price","DiscountPrice");

        //AXIS
        map.put("Service ID BOGOF","ServiceIdBogof");
        map.put("Service ID SIMPLY RED","ServiceIdSimplyRed");




        return map;
    }
}
